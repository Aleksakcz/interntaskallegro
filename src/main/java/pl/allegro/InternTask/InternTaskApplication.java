package pl.allegro.InternTask;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.allegro.InternTask.entity.AccountEntity;
import pl.allegro.InternTask.service.AccountService;
import pl.allegro.InternTask.service.LogService;
import pl.allegro.InternTask.service.UtilsService;

import java.util.ArrayList;
import java.util.Date;

@SpringBootApplication
public class InternTaskApplication implements CommandLineRunner {
	private final UtilsService utilsService;
	private final LogService logService;
	private final AccountService accountService;

	public InternTaskApplication(UtilsService utilsService, LogService logService, AccountService accountService) {
		this.utilsService = utilsService;
		this.logService = logService;
		this.accountService = accountService;
	}

	public static void main(String[] args){
		SpringApplication.run(InternTaskApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {
		utilsService.printMachineConnectionData();
		makeDummyData();
	}

	private void makeDummyData(){
		logService.addLog(LogService.INFO_LEVEL, "Adding dummy data for testing", null);
		AccountEntity privateAccount1 = new AccountEntity();
		privateAccount1.setName("Jan Kowalski");
		privateAccount1.setAccountId(1234);
		privateAccount1.setEmail("jan.kowalski@example.pl");
		privateAccount1.setLogin("jan1234");
		privateAccount1.setPhone("123456789");
		privateAccount1.setTaxId(null);
		privateAccount1.setCreated(new Date());
		privateAccount1.setEdited(new Date());

		AccountEntity privateAccount2 = new AccountEntity();
		privateAccount2.setName("Jan Kowalski, konto 2");
		privateAccount2.setAccountId(null);
		privateAccount2.setEmail("jan.kowalski2@example.pl");
		privateAccount2.setLogin("janek 6789");
		privateAccount2.setPhone("123456789");
		privateAccount2.setTaxId(null);
		privateAccount2.setCreated(new Date());
		privateAccount2.setEdited(new Date());

		AccountEntity privateAccount3 = new AccountEntity();
		privateAccount3.setName("Zbigniew Nowak");
		privateAccount3.setAccountId(null);
		privateAccount3.setEmail("zbigniew.test@example.pl");
		privateAccount3.setLogin("zbigniew234");
		privateAccount3.setPhone("123454449");
		privateAccount3.setTaxId(null);
		privateAccount3.setCreated(new Date());
		privateAccount3.setEdited(new Date());

		AccountEntity privateAccount4 = new AccountEntity();
		privateAccount4.setName("Zbigniew Kaszubski");
		privateAccount4.setAccountId(345);
		privateAccount4.setEmail("zbigniew.kaszubski@example.pl");
		privateAccount4.setLogin("zbignieWk");
		privateAccount4.setPhone("122235562");
		privateAccount4.setTaxId(null);
		privateAccount4.setCreated(new Date());
		privateAccount4.setEdited(new Date());

		AccountEntity privateAccount5 = new AccountEntity();
		privateAccount5.setName("Zbigniew Kaszubski");
		privateAccount5.setAccountId(5555);
		privateAccount5.setEmail("zbigniew.kaszubski.ale.inny@example.pl");
		privateAccount5.setLogin("zbigniewinny");
		privateAccount5.setPhone("12223333312");
		privateAccount5.setTaxId(null);
		privateAccount5.setCreated(new Date());
		privateAccount5.setEdited(new Date());

		AccountEntity companyAccount1 = new AccountEntity();
		companyAccount1.setName("Jan Nowak Firma");
		companyAccount1.setAccountId(5678);
		companyAccount1.setEmail("firma1@example.pl");
		companyAccount1.setLogin("firma1");
		companyAccount1.setPhone("12223333312");
		companyAccount1.setTaxId("832-123-233");
		companyAccount1.setCreated(new Date());
		companyAccount1.setEdited(new Date());

		AccountEntity companyAccount2 = new AccountEntity();
		companyAccount2.setName("Jan Nowak Firma2");
		companyAccount2.setAccountId(null);
		companyAccount2.setEmail("firma1_drugi adres@example.pl");
		companyAccount2.setLogin("firma1_drugiekonto");
		companyAccount2.setPhone("12253124772");
		companyAccount2.setTaxId("832-123-233");
		companyAccount2.setCreated(new Date());
		companyAccount2.setEdited(new Date());

		AccountEntity companyAccount3 = new AccountEntity();
		companyAccount3.setName("Trzecia niezalezna firma");
		companyAccount3.setAccountId(null);
		companyAccount3.setEmail("nowa firma@example.pl");
		companyAccount3.setLogin("nowaFirma");
		companyAccount3.setPhone("12252124772");
		companyAccount3.setTaxId("111-122-222");
		companyAccount3.setCreated(new Date());
		companyAccount3.setEdited(new Date());

		ArrayList<AccountEntity> dummyData = new ArrayList<>();
		dummyData.add(companyAccount1);
		dummyData.add(companyAccount2);
		dummyData.add(companyAccount3);
		dummyData.add(privateAccount1);
		dummyData.add(privateAccount2);
		dummyData.add(privateAccount3);
		dummyData.add(privateAccount4);
		dummyData.add(privateAccount5);

		accountService.addAccounts(dummyData);
		logService.addLog(LogService.INFO_LEVEL, "Successfully inserted "+ dummyData.size() + " records in database.", null);
	}
}
