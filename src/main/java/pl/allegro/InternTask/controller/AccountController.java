package pl.allegro.InternTask.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.allegro.InternTask.error.*;
import pl.allegro.InternTask.model.AccountInput;
import pl.allegro.InternTask.model.AccountOutput;
import pl.allegro.InternTask.model.UserOutput;
import pl.allegro.InternTask.service.AccountService;
import pl.allegro.InternTask.service.LogService;
import pl.allegro.InternTask.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;

@Controller
@CrossOrigin
@Api(value = "CRM Management")
@RequestMapping(value = "/api/crm/")
public class AccountController {
    private final AccountService accountService;
    private final UserService userService;
    private final LogService logService;

    public AccountController(AccountService accountService, UserService userService, LogService logService) {
        this.accountService = accountService;
        this.userService = userService;
        this.logService = logService;
    }

    @ApiOperation(value = "Add single account to CRM system", response = AccountOutput.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added/updated account", response = AccountOutput.class),
            @ApiResponse(code = 400, message = "Invalid account object", response = ErrorResponse.class),
            @ApiResponse(code = 409, message = "Account data can cause data disintegrate", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Unknown internal error", response = ErrorResponse.class)
    })
    @RequestMapping(value = "account", method = RequestMethod.POST)
    public ResponseEntity<Object> addUser(@RequestBody @Valid AccountInput accountInput, HttpServletRequest request) throws InvalidParameterException, EmptyAccountInputException, EmptyParameterException, DuplicatedAccountException {
        logService.addConnectionLog(request);
        return new ResponseEntity<>(accountService.addAccountByController(accountInput), HttpStatus.OK);
    }

    @ApiOperation(value = "Add multiple accounts to CRM system", response = AccountOutput.class, responseContainer="List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added/updated accounts", response = AccountOutput.class),
            @ApiResponse(code = 400, message = "Invalid account object", response = ErrorResponse.class),
            @ApiResponse(code = 409, message = "Account data can cause data disintegrate", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Unknown internal error", response = ErrorResponse.class)
    })
    @RequestMapping(value = "accounts", method = RequestMethod.POST)
    public ResponseEntity<Object> addUsers(@RequestBody @Valid ArrayList<AccountInput> accountsInput, HttpServletRequest request) throws InvalidParameterException, EmptyParameterException, EmptyAccountInputException, DuplicatedAccountException {
        logService.addConnectionLog(request);
        return new ResponseEntity<>(accountService.addAccountsByController(accountsInput), HttpStatus.OK);
    }

    @ApiOperation(value = "Update database structure", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated accounts and users structure"),
            @ApiResponse(code = 400, message = "Invalid account object", response = ErrorResponse.class),
            @ApiResponse(code = 409, message = "Account data can cause data disintegrate", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Unknown internal error", response = ErrorResponse.class)
    })
    @RequestMapping(value = "updateDatabase", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateDatabase(HttpServletRequest request) {
        logService.addConnectionLog(request);
        accountService.updateDatabase();
        return new ResponseEntity<>("[UPDATED]", HttpStatus.OK);
    }


    @ApiOperation(value = "Get all users with their accounts", response = UserOutput.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully sended users list"),
            @ApiResponse(code = 500, message = "Unknown internal error", response = ErrorResponse.class)
    })
    @RequestMapping(value = "users", method = RequestMethod.GET)
    public ResponseEntity<Object> getAllUsers(HttpServletRequest request) {
        logService.addConnectionLog(request);
        return new ResponseEntity<>(userService.getAllUsersByController(), HttpStatus.OK);
    }

}
