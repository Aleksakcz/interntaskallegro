package pl.allegro.InternTask.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pl.allegro.InternTask.error.ApiError;
import pl.allegro.InternTask.error.ErrorResponse;
import pl.allegro.InternTask.service.LogService;

@ControllerAdvice
public class GlobalControllerAdvice extends ResponseEntityExceptionHandler {
    private final LogService logService;

    public GlobalControllerAdvice(LogService logService) {
        this.logService = logService;
    }

    @ExceptionHandler(ApiError.class)
    ResponseEntity<Object> handleAllApiExceptions(ApiError e){
        logService.addLog(LogService.ERROR_LEVEL, e.getErrorMessage(), e.getClass().getName());
        return new ResponseEntity<>(new ErrorResponse(e), e.getHttpStatus());
    }
}