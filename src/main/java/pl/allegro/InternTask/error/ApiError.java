package pl.allegro.InternTask.error;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class ApiError extends Exception {
    private Integer errorId = 1000;
    private String errorTag = "[UNKNOWN_ERROR}";
    private String errorMessage = "Unknown error while processing Error";
    private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

    public ApiError(Integer errorId, String errorTag, String errorMessage, HttpStatus httpStatus) {
        this.errorId = errorId;
        this.errorTag = errorTag;
        this.errorMessage = errorMessage;
        this.httpStatus = httpStatus;
    }
}
