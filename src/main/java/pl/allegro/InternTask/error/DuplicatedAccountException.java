package pl.allegro.InternTask.error;

import org.springframework.http.HttpStatus;

public class DuplicatedAccountException extends ApiError {
    public DuplicatedAccountException(String parameter, String value, Integer dbDuplicatedId) {
        super(
                1004,
                "[DUPLICATED_ACCOUNT_EXCEPTION}",
                "Account with unique parameter [" + parameter + "] with value=\"" + value + "\" exist in database on position " + dbDuplicatedId + ".",
                HttpStatus.CONFLICT
        );
    }
}
