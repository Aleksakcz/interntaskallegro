package pl.allegro.InternTask.error;

import org.springframework.http.HttpStatus;

public class EmptyParameterException extends ApiError {
    public EmptyParameterException(String missingParameter) {
        super(
                1002,
                "[EMPTY_PARAMETER_EXCEPTION]",
                "Parameter [" + missingParameter + "] shouldn't be empty or null",
                HttpStatus.BAD_REQUEST
        );
    }
}
