package pl.allegro.InternTask.error;

import lombok.Data;

@Data
public class ErrorResponse {
    private Integer errorId;
    private String errorTag;
    private String errorMessage;

    public ErrorResponse(ApiError apiError) {
        this.errorId = apiError.getErrorId();
        this.errorTag = apiError.getErrorTag();
        this.errorMessage = apiError.getErrorMessage();
    }
}
