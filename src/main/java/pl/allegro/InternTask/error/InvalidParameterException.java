package pl.allegro.InternTask.error;

import org.springframework.http.HttpStatus;

public class InvalidParameterException extends ApiError {
    public InvalidParameterException(String invalidParameter, String cause) {
        super(
                1003,
                "[INVALID_PARAMETER_EXCEPTION]",
                "Parameter [" + invalidParameter + "] is invalid because " + cause + ".",
                HttpStatus.BAD_REQUEST
        );
    }
}
