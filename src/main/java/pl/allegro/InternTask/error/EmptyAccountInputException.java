package pl.allegro.InternTask.error;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;


@EqualsAndHashCode(callSuper = true)
@Data
public class EmptyAccountInputException extends ApiError {
    public EmptyAccountInputException(){
        super(
                1001,
                "[EMPTY_ACCOUNT_EXCEPTION]",
                "Account object shouldn't be empty or null",
                HttpStatus.BAD_REQUEST
        );
    }
}
