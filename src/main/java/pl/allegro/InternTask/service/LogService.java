package pl.allegro.InternTask.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Service
public class LogService {
    private final UtilsService utilsService;
    public final static int INFO_LEVEL = 3;
    public final static int CONNECTION_LEVEL = 2;
    public final static int ERROR_LEVEL = 1;

    @Value("${allegro.log.printlog}")
    Boolean printLog;
    @Value("${allegro.log.errorLevel}")
    private Integer registerErrorLevel;

    public LogService(@Lazy UtilsService utilsService) {
        this.utilsService = utilsService;
    }

    public void addLog(Integer errorLevel, String message, String method) {
        if (registerErrorLevel >= errorLevel) {
            StringBuilder sb = new StringBuilder();
            switch (errorLevel) {
                case 1:
                    sb.append("[ERROR] ");
                    break;
                case 3:
                    sb.append("[INFO] ");
                    break;
            }
            sb.append(new Date().toString());
            sb.append(" ");
            sb.append(message);
            if (method != null) {
                sb.append(" | caller: ");
                sb.append(method);
            }
            if (printLog) System.out.println(sb.toString());

            //TODO here can be implemented any logging service to database
        }
    }

    public void addConnectionLog(HttpServletRequest request) {
        if(registerErrorLevel >= CONNECTION_LEVEL){
            StringBuilder sb = new StringBuilder();
            sb.append("[CONNECTION] ");
            sb.append(new Date().toString());
            sb.append(" From: ");
            sb.append(utilsService.getIpAddress(request));
            sb.append(" To: ");
            sb.append(request.getRequestURL().toString());
            sb.append("[");
            sb.append(request.getMethod());
            sb.append("]");

            //TODO here can be implemented any logging service to database

            if(printLog){
                System.out.println(sb.toString());
            }

        }
    }
}
