package pl.allegro.InternTask.service;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import pl.allegro.InternTask.entity.AccountEntity;
import pl.allegro.InternTask.entity.UserEntity;
import pl.allegro.InternTask.model.UserOutput;
import pl.allegro.InternTask.repository.UserRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final AccountService accountService;
    private final LogService logService;

    public ArrayList<UserOutput> getAllUsersByController() {
        ArrayList<UserEntity> userEntities = (ArrayList<UserEntity>) userRepository.findAll();
        return userEntitiesToOutputs(userEntities);
    }

    public UserService(UserRepository userRepository, @Lazy AccountService accountService, LogService logService) {
        this.userRepository = userRepository;
        this.accountService = accountService;
        this.logService = logService;
    }


    private ArrayList<UserOutput> userEntitiesToOutputs(ArrayList<UserEntity> userEntities) {
        ArrayList<UserOutput> userOutputs = new ArrayList<>();

        for (UserEntity userEntity : userEntities) {
            userOutputs.add(userEntityToOutput(userEntity));
        }
        logService.addLog(LogService.INFO_LEVEL, "Retrieved users " + userOutputs.size(), "userEntitiesToOutputs()");
        return userOutputs;
    }

    public Optional<UserEntity> getUserByTaxId(String taxId) {
        return userRepository.findByTaxId(taxId);
    }

    public Optional<UserEntity> getUserByPhoneNumber(String phoneNumber) {
        return userRepository.findByPhoneAndIsCompanyIsFalse(phoneNumber);
    }

    public UserEntity createNewCompanyUser(AccountEntity accountEntity) {
        UserEntity userEntity = new UserEntity();

        userEntity.setTaxId(accountEntity.getTaxId());
        userEntity.setIsCompany(true);

        userEntity.setCreated(new Date());
        userEntity.setEdited(new Date());

        userRepository.save(userEntity);
        return userEntity;
    }

    public UserEntity createNewPrivateUser(AccountEntity accountEntity) {
        UserEntity userEntity = new UserEntity();

        userEntity.setPhone(accountEntity.getPhone());
        userEntity.setIsCompany(false);

        userEntity.setCreated(new Date());
        userEntity.setEdited(new Date());

        userRepository.save(userEntity);
        return userEntity;
    }

    private UserOutput userEntityToOutput(UserEntity userEntity) {
        UserOutput userOutput = new UserOutput();

        userOutput.setId(userEntity.getId());
        userOutput.setIsCompany(userEntity.getIsCompany());
        userOutput.setPhone(userEntity.getPhone());
        userOutput.setTaxId(userEntity.getTaxId());
        userOutput.setCreated(userEntity.getCreated());
        userOutput.setEdited(userEntity.getEdited());
        userOutput.setAccountOutputs(new ArrayList<>());

        if (userEntity.getAccountEntities() != null)
            for (AccountEntity accountEntity : userEntity.getAccountEntities()) {
                userOutput.getAccountOutputs().add(accountService.accountEntityToOutput(accountEntity));
            }

        return userOutput;
    }
}
