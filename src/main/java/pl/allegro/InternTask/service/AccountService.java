package pl.allegro.InternTask.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.allegro.InternTask.entity.AccountEntity;
import pl.allegro.InternTask.entity.UserEntity;
import pl.allegro.InternTask.error.*;
import pl.allegro.InternTask.model.AccountInput;
import pl.allegro.InternTask.model.AccountOutput;
import pl.allegro.InternTask.repository.AccountRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

@Service
public class AccountService {
    private final AccountRepository accountRepository;
    private final UtilsService utilsService;
    private final UserService userService;
    private final LogService logService;

    @Value("${allegro.runtime.stopMultipleAddingWhenError}")
    private Boolean stopMultipleAddingWhenError;

    public AccountService(AccountRepository accountRepository, UtilsService utilsService, UserService userService, LogService logService) {
        this.accountRepository = accountRepository;
        this.utilsService = utilsService;
        this.userService = userService;
        this.logService = logService;
    }


    public AccountOutput addAccountByController(AccountInput accountInput) throws EmptyAccountInputException, EmptyParameterException, InvalidParameterException, DuplicatedAccountException {
        AccountEntity accountEntity = accountInputToEntity(accountInput);
        logService.addLog(LogService.INFO_LEVEL, "Adding account " + accountEntity.getAccountId(), "addAccountByController()");
        addAccountToDatabase(accountEntity);
        logService.addLog(LogService.INFO_LEVEL, "Adding one account done", "addAccountByController()");
        return accountEntityToOutput(accountEntity);
    }

    public ArrayList<AccountOutput> addAccountsByController(ArrayList<AccountInput> accountsInput) throws EmptyAccountInputException, EmptyParameterException, InvalidParameterException, DuplicatedAccountException {
        ArrayList<AccountEntity> accountEntities = new ArrayList<>();
        AccountEntity accountEntity;
        logService.addLog(LogService.INFO_LEVEL, "Adding " + accountsInput.size() + " accounts.", "addAccountsByController()");
        for (AccountInput accountInput : accountsInput) {
            try {
                accountEntity = accountInputToEntity(accountInput);
                addAccountToDatabase(accountEntity);
                accountEntities.add(accountEntity);
                logService.addLog(LogService.INFO_LEVEL, "Add account " + accountEntity.getAccountId() + " successfully", "addAccountsByController()");

            } catch (ApiError error) {
                if (stopMultipleAddingWhenError) {
                    throw error;
                } else {
                    logService.addLog(LogService.ERROR_LEVEL, error.getErrorMessage() + " but continuing", "addAccountsByController()");
                }
            }

        }
        logService.addLog(LogService.INFO_LEVEL, "Adding multiple accounts done. Received = " + accountsInput.size() + " Valid= " + accountEntities.size(), "addAccountsByController()");
        return accountEntitiesToOutputs(accountEntities);
    }

    @Transactional
    public void addAccountToDatabase(AccountEntity accountEntity) throws DuplicatedAccountException {
        checkAndUpdateAccounts(accountEntity);
    }

    private void checkAndUpdateAccounts(AccountEntity accountEntity) throws DuplicatedAccountException {
        Optional<AccountEntity> duplicatedEntity = accountRepository.findByAccountId(accountEntity.getAccountId());
        if (duplicatedEntity.isPresent()) {
            logService.addLog(LogService.INFO_LEVEL, "Account with specified id found. Merging " + duplicatedEntity.get().getAccountId(), "checkAndUpdateAccounts()");
            accountEntity.setDbId(duplicatedEntity.get().getDbId());
            accountEntity.setCreated(duplicatedEntity.get().getCreated());
            duplicatedEntity = accountRepository.findByLoginAndDbIdIsNot(accountEntity.getLogin(), accountEntity.getDbId());
        } else {
            duplicatedEntity = accountRepository.findByLogin(accountEntity.getLogin());
        }

        if (duplicatedEntity.isPresent()) {
            if (duplicatedEntity.get().getAccountId() != null) {
                throw new DuplicatedAccountException("login", duplicatedEntity.get().getLogin(), duplicatedEntity.get().getDbId());
            } else {
                logService.addLog(LogService.INFO_LEVEL, "Updating Account " + duplicatedEntity.get().getLogin(), "checkAndUpdateAccounts()");
                accountEntity.setDbId(duplicatedEntity.get().getDbId());
            }
        }

        makeEmailUniqueToSpecifiedAccount(accountEntity);

        createOrFindUserRelationForAccount(accountEntity);
        accountRepository.save(accountEntity);
    }

    private AccountEntity createOrFindUserRelationForAccount(AccountEntity accountEntity) {
        if (accountBelongsToCompany(accountEntity)) {

            Optional<UserEntity> userEntity = userService.getUserByTaxId(accountEntity.getTaxId());
            if (userEntity.isEmpty()) {

                UserEntity newUser = userService.createNewCompanyUser(accountEntity);
                logService.addLog(LogService.INFO_LEVEL, "Creating new company user " + newUser.getTaxId() + " for account: " + accountEntity.getLogin(), "createOrFindUserRelationForAccount()");
                accountEntity.setUserEntity(newUser);
            } else {
                logService.addLog(LogService.INFO_LEVEL, "Using existing company user " + userEntity.get().getTaxId() + " for account: " + accountEntity.getLogin(), "createOrFindUserRelationForAccount()");
                userEntity.get().setEdited(new Date());
                accountEntity.setUserEntity(userEntity.get());
            }
        } else {
            Optional<UserEntity> userEntity = userService.getUserByPhoneNumber(accountEntity.getPhone());
            if (userEntity.isEmpty()) {
                UserEntity newUser = userService.createNewPrivateUser(accountEntity);
                logService.addLog(LogService.INFO_LEVEL, "Creating new private user " + newUser.getPhone() + " for account: " + accountEntity.getLogin(), "createOrFindUserRelationForAccount()");
                accountEntity.setUserEntity(newUser);
            } else {
                accountEntity.setUserEntity(userEntity.get());
                logService.addLog(LogService.INFO_LEVEL, "Using existing private user " + userEntity.get().getPhone() + " for account: " + accountEntity.getLogin(), "createOrFindUserRelationForAccount()");
                userEntity.get().setEdited(new Date());
            }
        }
        return accountEntity;
    }

    private void makeEmailUniqueToSpecifiedAccount(AccountEntity accountEntity) {
        ArrayList<AccountEntity> accountsWithIdenticalEmail;
        if (accountEntity.getDbId() == null) {
            accountsWithIdenticalEmail = accountRepository.findAllByEmail(accountEntity.getEmail());
        } else {
            accountsWithIdenticalEmail = accountRepository.findAllByEmailAndDbIdIsNotAndIsMailActiveIsTrue(accountEntity.getEmail(), accountEntity.getDbId());
        }

        for (AccountEntity account : accountsWithIdenticalEmail) {
            logService.addLog(LogService.INFO_LEVEL, "Setting email " + account.getEmail() + " for account: " + account.getLogin() + " as historical.", "makeEmailUniqueToSpecifiedAccount()");
            account.setIsMailActive(false);
            account.setEdited(new Date());
            accountRepository.save(account);
        }
    }

    public void addAccounts(ArrayList<AccountEntity> accountEntities){
        accountRepository.saveAll(accountEntities);
    }

    private Boolean accountBelongsToCompany(AccountEntity accountEntity) {
        return !utilsService.isBlankOrNull(accountEntity.getTaxId());
    }
    private void checkAccountInput(AccountInput accountInput) throws EmptyAccountInputException, EmptyParameterException, InvalidParameterException {
        if (accountInput == null) throw new EmptyAccountInputException();
        if (utilsService.isBlankOrNull(accountInput.getPhone())) throw new EmptyParameterException("phone");
        if (utilsService.isBlankOrNull(accountInput.getAddress())) throw new EmptyParameterException("address");
        if (accountInput.getAccountId() == null) throw new EmptyParameterException("id");
        if (utilsService.isBlankOrNull(accountInput.getName())) throw new EmptyParameterException("name");
        if (utilsService.isBlankOrNull(accountInput.getEmail())) throw new EmptyParameterException("email");
        utilsService.checkIfEmailIsValid(accountInput.getEmail());

    }

    private ArrayList<AccountOutput> accountEntitiesToOutputs(ArrayList<AccountEntity> accountEntities) {
        ArrayList<AccountOutput> accountOutputs = new ArrayList<>();

        for (AccountEntity accountEntity : accountEntities) {
            accountOutputs.add(accountEntityToOutput(accountEntity));
        }

        return accountOutputs;
    }

    AccountOutput accountEntityToOutput(AccountEntity accountEntity) {
        AccountOutput accountOutput = new AccountOutput();

        accountOutput.setDbId(accountEntity.getDbId());
        accountOutput.setAccountId(accountEntity.getAccountId());
        accountOutput.setName(accountEntity.getName());
        accountOutput.setTaxId(accountEntity.getTaxId());
        accountOutput.setEmail(accountEntity.getEmail());
        accountOutput.setIsMailActive(accountEntity.getIsMailActive());
        accountOutput.setPhone(accountEntity.getPhone());
        accountOutput.setPhoneSecond(accountEntity.getPhoneSecond());
        accountOutput.setLogin(accountEntity.getLogin());
        accountOutput.setAddress(accountEntity.getAddress());
        accountOutput.setCreated(accountEntity.getCreated());
        accountOutput.setEdited(accountEntity.getEdited());

        return accountOutput;
    }

    private AccountEntity accountInputToEntity(AccountInput accountInput) throws EmptyAccountInputException, EmptyParameterException, InvalidParameterException {
        checkAccountInput(accountInput);
        AccountEntity accountEntity = new AccountEntity();

        accountEntity.setAccountId(accountInput.getAccountId());
        accountEntity.setName(accountInput.getName());
        accountEntity.setTaxId(accountInput.getTaxId());
        accountEntity.setEmail(accountInput.getEmail());
        accountEntity.setIsMailActive(true);
        accountEntity.setPhone(accountInput.getPhone());
        accountEntity.setPhoneSecond(accountInput.getPhoneSecond());
        accountEntity.setLogin(accountInput.getLogin());
        accountEntity.setAddress(accountInput.getAddress());
        accountEntity.setCreated(new Date());
        accountEntity.setEdited(new Date());

        return accountEntity;
    }

    public void updateDatabase() {
        logService.addLog(LogService.INFO_LEVEL, "Updating database structure", "updateDatabase()");
        ArrayList<AccountEntity> accountEntities = accountRepository.findAllByUserEntityIsNull();
        if (accountEntities.size() != 0) {
            logService.addLog(LogService.INFO_LEVEL, "Updating database structure for " + accountEntities.size() + " accounts", "updateDatabase()");
        } else {
            logService.addLog(LogService.INFO_LEVEL, "Nothing to update. Accounts have valid structure.", "updateDatabase()");
        }
        for (AccountEntity accountEntity : accountEntities) {
            logService.addLog(LogService.INFO_LEVEL, "Updating account " + accountEntity.getLogin(), "updateDatabase()");
            createOrFindUserRelationForAccount(accountEntity);
            accountRepository.save(accountEntity);
        }
    }
}
