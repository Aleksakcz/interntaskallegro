package pl.allegro.InternTask.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.allegro.InternTask.error.InvalidParameterException;

import javax.servlet.http.HttpServletRequest;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UtilsService {
    private final Pattern EMAIL_VALID_PATTERN = Pattern.compile("^(.+)@(.+)$");
    private final LogService logService;

    @Value("${allegro.variables.maxEmailLength}")
    private Integer MAX_EMAIL_LENGTH;

    @Value("${allegro.variables.minEmailLength}")
    private Integer MIN_EMAIL_LENGTH;

    @Value("${server.port}")
    private int serverPort;

    @Value("${spring.datasource.url}")
    private String dbName;

    @Value("${spring.datasource.username}")
    private String dbUser;

    @Value("${spring.datasource.password}")
    private String dbPassword;

    @Value("${spring.datasource.driverClassName}")
    private String dbDriver;

    public UtilsService(LogService logService) {
        this.logService = logService;
    }

    public void checkIfEmailIsValid(String email) throws InvalidParameterException {
        if (email.length() > MAX_EMAIL_LENGTH) {
            throw new InvalidParameterException("email", " email is too long");
        } else if (email.length() < MIN_EMAIL_LENGTH) {
            throw new InvalidParameterException("email", " email is too short");
        } else {
            Matcher matcher = EMAIL_VALID_PATTERN.matcher(email);
            if (!matcher.matches()) {
                throw new InvalidParameterException("email", "email has invalid structure");
            }
        }

    }

    public String getCurrentIp() throws UnknownHostException {
        return Inet4Address.getLocalHost().getHostAddress();
    }

    public boolean isBlankOrNull(String str) {
        return str == null || str.isBlank();
    }

    public void printMachineConnectionData() {
        try {
            String currentAddress =  getCurrentIp() + ":"+this.serverPort;
            logService.addLog(LogService.INFO_LEVEL, "Server is running on address: "+ currentAddress, null);
            logService.addLog(LogService.INFO_LEVEL, "Swagger API documentation: http://"+ currentAddress + "/swagger-ui.html", null);
            logService.addLog(LogService.INFO_LEVEL, "=============== DB DATA =====================" , null);
            logService.addLog(LogService.INFO_LEVEL, "DB Driver: " + dbDriver , null);
            logService.addLog(LogService.INFO_LEVEL, "DB URL: http://" + currentAddress +"/h2-console", null);
            logService.addLog(LogService.INFO_LEVEL, "DB Name: " + dbName , null);
            logService.addLog(LogService.INFO_LEVEL, "DB User: " + dbUser, null);
            logService.addLog(LogService.INFO_LEVEL, "DB Password: " + dbPassword, null);
            logService.addLog(LogService.INFO_LEVEL, "=============================================" , null);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

    public String getIpAddress(HttpServletRequest httpServletRequest) {
        String ip = httpServletRequest.getRemoteAddr();
        if (ip.equalsIgnoreCase("0:0:0:0:0:0:0:1")) {
            try {
                return InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                return "IP-UNKNOWN";
            }
        }
        return ip;
    }
}
