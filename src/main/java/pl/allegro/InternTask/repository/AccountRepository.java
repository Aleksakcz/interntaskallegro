package pl.allegro.InternTask.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.allegro.InternTask.entity.AccountEntity;

import java.util.ArrayList;
import java.util.Optional;

@Repository
public interface AccountRepository extends CrudRepository<AccountEntity, Integer> {
    Optional<AccountEntity> findByAccountId(Integer accountId);

    ArrayList<AccountEntity> findAllByUserEntityIsNull();

    Optional<AccountEntity> findByLoginAndDbIdIsNot(String login, Integer dbId);

    ArrayList<AccountEntity> findAllByEmailAndDbIdIsNotAndIsMailActiveIsTrue(String email, Integer dbId);

    Optional<AccountEntity> findByLogin(String login);

    ArrayList<AccountEntity> findAllByEmail(String email);
}
