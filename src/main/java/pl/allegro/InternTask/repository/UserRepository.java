package pl.allegro.InternTask.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.allegro.InternTask.entity.UserEntity;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Integer> {
    Optional<UserEntity> findByTaxId(String taxId);
    Optional<UserEntity> findByPhoneAndIsCompanyIsFalse(String taxId);
}
