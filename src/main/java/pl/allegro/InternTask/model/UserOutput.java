package pl.allegro.InternTask.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UserOutput {
    @ApiModelProperty(notes = "Id in CRM database", example = "2", required = true)
    private Integer id;

    @ApiModelProperty(notes = "Is user registered as company?", example = "true", required = true)
    private Boolean isCompany;

    @ApiModelProperty(notes = "User Tax number (if user is company)", example = "123-123-44-44", required = false)
    private String taxId;

    @ApiModelProperty(notes = "User phone (if user is private)", example = "123-345-567", required = false)
    private String phone;

    @ApiModelProperty(notes = "Created timestamp", required = true)
    private Date created;

    @ApiModelProperty(notes = "Edited timestamp", required = true)
    private Date edited;

    @ApiModelProperty(notes = "Accounts that belongs to user", required = true)
    private List<AccountOutput> accountOutputs;
}
