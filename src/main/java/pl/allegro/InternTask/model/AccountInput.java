package pl.allegro.InternTask.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AccountInput {
    @ApiModelProperty(notes = "Account ID in Allegro system", example = "1234", required = true)
    private Integer accountId;

    @ApiModelProperty(notes = "User name and surname", example = "John Doe", required = true)
    private String name;

    @ApiModelProperty(notes = "TaxId only if company account", example = "973-105-90-30", required = false)
    private String taxId;

    @ApiModelProperty(notes = "User email. Should be valid", example = "john.doe@exampl.com", required = true)
    private String email;

    @ApiModelProperty(notes = "User phone number", example = "123 456 789", required = true)
    private String phone;

    @ApiModelProperty(notes = "Additional user phone number", example = "123 123 123", required = false)
    private String phoneSecond;

    @ApiModelProperty(notes = "User login", example = "johnDoe97", required = true)
    private String login;

    @ApiModelProperty(notes = "User Address", example = "Bakery St. 12/13", required = true)
    private String address;
}
