package pl.allegro.InternTask.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity(name = "USER")
public class UserEntity {
    @Id
    @GeneratedValue
    private Integer id;

    private Boolean isCompany;
    private String taxId;
    private String phone;

    @Column(name = "created_date")
    private Date created;

    @Column(name = "edited_date")
    private Date edited;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userEntity")
    private List<AccountEntity> accountEntities;
}
