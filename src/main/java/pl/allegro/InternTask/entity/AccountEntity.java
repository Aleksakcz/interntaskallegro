package pl.allegro.InternTask.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity(name = "ACCOUNT")
public class AccountEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer dbId;

    private String name;
    private String taxId;
    private String email;

    @Column(length = 12)
    private String phone;

    @Column(name="phone_second", length = 12)
    private String phoneSecond;

    private Integer accountId;

    private String login;
    private String address;

    @Column(name = "is_email_active")
    private Boolean isMailActive;

    @Column(name = "created_date")
    private Date created;

    @Column(name = "edited_date")
    private Date edited;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;
}
